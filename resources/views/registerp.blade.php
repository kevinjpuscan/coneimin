<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Responsive CSS Tabs</title>

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  
  
  
      <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url("http://fonts.googleapis.com/css?family=Open+Sans:400,600,700");
@import url("http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css");


html, body {
  height: 100%;
}

body {
  font: 14px/1 'Open Sans', sans-serif;
  color: #555;
  background: #eee;
}

h1 {
  padding: 50px 0;
  font-weight: 400;
  text-align: center;
}

p {
  margin: 0 0 20px;
  line-height: 1.5;
}

main {
  min-width: 320px;
  max-width: 800px;
  padding: 50px;
  margin: 0 auto;
  background: #fff;
}

section {
  display: none;
  padding: 20px 0 0;
  border-top: 1px solid #ddd;
}

input {
  display: none;
}

label {
  display: inline-block;
  margin: 0 0 -1px;
  padding: 15px 25px;
  font-weight: 600;
  text-align: center;
  color: #bbb;
  border: 1px solid transparent;
}



label:hover {
  color: #888;
  cursor: pointer;
}

input:checked + label {
  color: #555;
  border: 1px solid #ddd;
  border-top: 2px solid orange;
  border-bottom: 1px solid #fff;
}

#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4 {
  display: block;
}


@media screen and (max-width: 400px) {
  label {
    padding: 15px;
  }
}

    </style>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

</head>

<body>
        <h1>Inscripciones</h1>
    
 

<main>
  
  <input id="tab1" type="radio" name="tabs" checked>
  <label for="tab1"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Congreso</label>
    
  <input id="tab2" type="radio" name="tabs">
  <label for="tab2"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span> Concursos</label>
    
  <section id="content1" style="text-align:left;">
    {!! Form::open(['method'=>'POST','route'=>'asistente.store','files' => 'true']) !!}
    	 <div class="form-group" >
		    <label for="">Dni:</label>
		    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="dni" required="true">
		  </div>
		<div class="form-group">
		    <label for="">Nombres y Apellidos:</label>
		    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" required="true">
		  </div>

		 <div class="form-group">
		    <label for="">Email:</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="" name="email" required="true">
		  </div>
		 

     <div class="form-group">
            <label for="" id="university">Universidad:</label>
           <select name="university" class="form-control">
              <option value="Seleccionar">Seleccionar</option>
              <option value="Universidad Nacional de Cajamarca">Universidad Nacional de Cajamarca</option>
              <option value="Profesional">Soy egresado</option>
           </select>
     </div>

     <div class="form-group">
            <label for="" id="lugar">Lugar de Procedencia:</label>
           <select name="place" class="form-control">
              <option value="Seleccionar">Seleccionar</option>
              <option value="Cajamarca">Cajamarca</option>
              <option value="Otro">Otro</option>
           </select>
     </div>

		 <div class="form-group">
		    <label for="">Voucher:</label>
		    {!! form::file('voucher',array('required'=>'true','class'=>'form-control'),['class' => 'form-control']) !!}
		  </div>
		 <center>
		 	<button type="submit" class="btn btn-warning" >Registrarse</button>
		 </center>

    {{Form::close()}}
  </section>
    
  <section id="content2" style="text-align:left;">
     {!! Form::open(['method'=>'POST','route'=>'concurso.store','files' => 'true']) !!}
     <div class="form-group">
            <label for="" id="competencia">Concurso:</label>
           <select name="competition" class="form-control">
              <option value="Seleccionar">Seleccionar</option>
              <option value="Caminata minera">Caminata minera</option>
              <option value="Profesional">Soy egresado</option>
           </select>
     </div>
     <div class="form-group" >
        <label for="">Dni:</label>
        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="dni" required="true">
      </div>
    <div class="form-group">
        <label for="">Nombres y Apellidos:</label>
        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" required="true">
      </div>

     <div class="form-group">
        <label for="">Email:</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="" name="email" required="true">
      </div>
     

     <div class="form-group">
            <label for="" id="university">Universidad:</label>
           <select name="university" class="form-control">
              <option value="Seleccionar">Seleccionar</option>
              <option value="Universidad Nacional de Cajamarca">Universidad Nacional de Cajamarca</option>
              <option value="Profesional">Soy egresado</option>
           </select>
     </div>

     <div class="form-group">
            <label for="" id="lugar">Lugar de Procedencia:</label>
           <select name="place" class="form-control">
              <option value="Seleccionar">Seleccionar</option>
              <option value="Cajamarca">Cajamarca</option>
              <option value="Otro">Otro</option>
           </select>
     </div>

     <div class="form-group">
        <label for="">Voucher:</label>
        {!! form::file('voucher',array('required'=>'true','class'=>'form-control'),['class' => 'form-control']) !!}
      </div>
     <center>
      <button type="submit" class="dalej" >Registrarse</button>
     </center>

    {{Form::close()}}
  </section>
    
</main>
  
  
</body>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>

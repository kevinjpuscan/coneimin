@extends('layouts.public')

@section('title',' Consulta')

@section('scriptSuperior')
                <!-- Latest compiled and minified CSS -->

                
                  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">    
                  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                  <script>
                  $( function() {
                    $( "#tabs" ).tabs();
                  } );
                </script>
@endsection

@section('content')
<div class='container'>
            <div class='slogan'>
                <div class='ten columns'>
                    <h1>INSCRIPCIONES</h1>
                    <h2>Registrate y participa de esta increible experiencia</h2>
                </div>
               

            </div>
        </div>  
    </header>


   
        <div class="container" style="position: relative;">
            <div class="sixteen columns">
                
               
            <div id="tabs" style="margin: 30px 0px 0 20px;">
                  <ul>
                      
                     
                        <li><a href="#congreso" type="submit" class="btn-tabs">Congreso</a></li>
                        
                    
                        <li><a href="#concurso" type="submit" class="btn-tabs">Concurso</a></li>
                        
                    
                    
                  </ul>
                  <div id="congreso">
                    <section id="content1" style="text-align:left;">
                    {!! Form::open(['method'=>'POST','route'=>'asistente.store','files' => 'true']) !!}
                         <div class="form-group" >
                            <label for="">Dni:</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="dni" required="true">
                          </div>
                        <div class="form-group">
                            <label for="">Nombres y Apellidos:</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" required="true">
                          </div>

                         <div class="form-group">
                            <label for="">Email:</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="" name="email" required="true">
                          </div>
                         

                     <div class="form-group">
                            <label for="" id="university">Universidad:</label>
                           <select name="university" class="form-control">
                              <option value="Seleccionar">Seleccionar</option>
                              <option value="UNIVERSIDAD NACIONAL SANTIAGO ANTUNEZ DE MAYOLO">UNIVERSIDAD NACIONAL SANTIAGO ANTUNEZ DE MAYOLO</option>
                              <option value="UNIVERSIDAD NACIONAL DEL SANTA"> UNIVERSIDAD NACIONAL DEL SANTA</option>
                              <option value="UNIVERSIDAD NACIONAL DE TRUJILLO"> UNIVERSIDAD NACIONAL DE TRUJILLO</option>
                              <option value="UNIVERSIDAD NACIONAL DE INGENIERIA"> UNIVERSIDAD NACIONAL DE INGENIERIA</option>
                              <option value="UNIVERSIDAD LOS ANGELES DE CHIMBOTE"> UNIVERSIDAD LOS ANGELES DE CHIMBOTE</option>
                              <option value="UNIVERSIDAD SAN PEDRO"> UNIVERSIDAD SAN PEDRO</option>
                              <option value="UNIVERSIDAD PRIVADA ANTENOR ORREGO"> UNIVERSIDAD PRIVADA ANTENOR ORREGO</option>
                              <option value="PONTIFICIA UNIVERSIDAD CATOLICA DEL PERU"> PONTIFICIA UNIVERSIDAD CATOLICA DEL PERU</option>
                              <option value="UNIVERSIDAD NACIONAL DE CAJAMARCA"> UNIVERSIDAD NACIONAL DE CAJAMARCA</option>
                              <option value="UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS"> UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS</option>
                              <option value="UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN"> UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN</option>
                              <option value="UNIVERSIDAD PERUANA DE ARTE ORVAL"> UNIVERSIDAD PERUANA DE ARTE ORVAL</option>
                              <option value="UNIVERSIDAD CIENTIFICA DEL SUR"> UNIVERSIDAD CIENTIFICA DEL SUR</option>
                              <option value="UNIVERSIDAD FEMENINA DEL SAGRADO CORAZON"> UNIVERSIDAD FEMENINA DEL SAGRADO CORAZON</option>
                              <option value="UNIVERSIDAD PRIVADA NORBERT WIENER"> UNIVERSIDAD PRIVADA NORBERT WIENER</option>
                              <option value="UNIVERSIDAD PRIVADA DEL NORTE"> UNIVERSIDAD PRIVADA DEL NORTE</option>
                              <option value="UNIVERSIDAD NACIONAL JOSE FAUSTINO SANCHEZ CARRION"> UNIVERSIDAD NACIONAL JOSE FAUSTINO SANCHEZ CARRION</option>
                              <option value="UNIVERSIDAD NACIONAL HERMILIO VALDIZAN"> UNIVERSIDAD NACIONAL HERMILIO VALDIZAN</option>
                              <option value="UNIVERSIDAD ANTONIO RUIZ DE MONTOYA"> UNIVERSIDAD ANTONIO RUIZ DE MONTOYA</option>
                              <option value="UNIVERSIDAD MARCELINO CHAMPAGNAT"> UNIVERSIDAD MARCELINO CHAMPAGNAT</option>
                              <option value="UNIVERSIDAD NACIONAL DE PIURA"> UNIVERSIDAD NACIONAL DE PIURA</option>
                              <option value="UNIVERSIDAD NACIONAL DEL CALLAO"> UNIVERSIDAD NACIONAL DEL CALLAO</option>
                              
                              <option value="UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS"> UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS</option>
                              <option value="UNIVERSIDAD CIENTIFICA DEL SUR"> UNIVERSIDAD CIENTIFICA DEL SUR</option>
                              <option value="UNIVERSIDAD CONTINENTAL"> UNIVERSIDAD CONTINENTAL</option>
                              <option value="UNIVERSIDAD NACIONAL SAN AGUSTIN"> UNIVERSIDAD NACIONAL SAN AGUSTIN</option>
                              <option value="UNIVERSIDAD CESAR VALLEJO"> UNIVERSIDAD CESAR VALLEJO</option>
                              <option value="UNIVERSIDAD PRIVADA NORBERT WIENER"> UNIVERSIDAD PRIVADA NORBERT WIENER</option>
                              <option value="UNIVERSIDAD NACIONAL FEDERICO VILLARREAL"> UNIVERSIDAD NACIONAL FEDERICO VILLARREAL</option>
                              <option value="ASOCIACION CULTURAL PERUANO BRITANICA"> ASOCIACION CULTURAL PERUANO BRITANICA</option>
                              <option value="UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN"> UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN</option>
                              <option value="UNIVERSIDAD NACIONAL DE SAN ANTONIO ABAD DEL CUSCO"> UNIVERSIDAD NACIONAL DE SAN ANTONIO ABAD DEL CUSCO</option>
                              <option value="UNIVERSIDAD NACIONAL PEDRO RUIZ GALLO"> UNIVERSIDAD NACIONAL PEDRO RUIZ GALLO</option>
                              <option value="CIBERTEC"> CIBERTEC</option>
                              <option value="UNIVERSIDAD NACIONAL DANIEL ALCIDES CARRION"> UNIVERSIDAD NACIONAL DANIEL ALCIDES CARRION</option>
                              <option value="UNIVERSIDAD NACIONAL DE MOQUEGUA"> UNIVERSIDAD NACIONAL DE MOQUEGUA</option>
                              <option value="UNIVERSIDAD PERUANA CAYETANO HEREDIA"> UNIVERSIDAD PERUANA CAYETANO HEREDIA</option>
                              <option value="UNIVERSIDAD PRIVADA SAN JUAN BAUTISTA"> UNIVERSIDAD PRIVADA SAN JUAN BAUTISTA</option>
                              <option value="UNIVERSIDAD NACIONAL DEL ALTIPLANO"> UNIVERSIDAD NACIONAL DEL ALTIPLANO</option>
                              <option value="UNIVERSIDAD NACIONAL SAN LUIS GONZAGA DE ICA"> UNIVERSIDAD NACIONAL SAN LUIS GONZAGA DE ICA</option>
                              <option value="UNIVERSIDAD TECNOLOGICA DEL PERU (UTP)"> UNIVERSIDAD TECNOLOGICA DEL PERU (UTP)</option>
                              <option value="UNIVERSIDAD NACIONAL MICAELA BASTIDAS DE APURIMAC"> UNIVERSIDAD NACIONAL MICAELA BASTIDAS DE APURIMAC</option>
                              <option value="OTROS">OTROS</option>
                              <option value="Profesional">Soy egresado</option>
                           </select>
                     </div>

                     <div class="form-group">
                            <label for="" id="lugar">Lugar de Procedencia:</label>
                           <select name="place" class="form-control">
                              <option value="Seleccionar">Seleccionar</option>
                              <option value=" AMAZONAS"> AMAZONAS</option>
                              <option value=" ANCASH"> ANCASH</option>
                              <option value=" APURIMAC"> APURIMAC</option>
                              <option value=" AREQUIPA"> AREQUIPA</option>
                              <option value=" AYACUCHO"> AYACUCHO</option>
                              <option value=" CAJAMARCA"> CAJAMARCA</option>
                              <option value=" CALLAO"> CALLAO</option>
                              <option value=" CUSCO"> CUSCO</option>
                              <option value=" HUANCAVELICA"> HUANCAVELICA</option>
                              <option value=" HUANUCO"> HUANUCO</option>
                              <option value=" ICA"> ICA</option>
                              <option value=" JUNIN"> JUNIN</option>
                              <option value=" LA LIBERTAD"> LA LIBERTAD</option>
                              <option value=" LAMBAYEQUE"> LAMBAYEQUE</option>
                              <option value=" LIMA METROPOLITANA"> LIMA METROPOLITANA</option>
                              <option value=" LIMA (REGIÓN)"> LIMA (REGIÓN)</option>
                              <option value=" LORETO"> LORETO</option>
                              <option value=" MADRE DE DIOS"> MADRE DE DIOS</option>
                              <option value=" MOQUEGUA"> MOQUEGUA</option>
                              <option value=" PASCO"> PASCO</option>
                              <option value=" PIURA"> PIURA</option>
                              <option value=" PUNO"> PUNO</option>
                              <option value=" SAN MARTIN"> SAN MARTIN</option>
                              <option value=" TACNA"> TACNA</option>
                              <option value=" TUMBES"> TUMBES</option>
                              <option value=" UCAYALI"> UCAYALI</option>
        
                              <option value="OTRO">OTRO</option>
                           </select>
                     </div>

                         <div class="form-group">
                            <label for="">Voucher:</label>
                            {!! form::file('voucher',array('required'=>'true','class'=>'form-control'),['class' => 'form-control']) !!}
                          </div>
                         <center>
                            <button type="submit" class="btn btn-warning" >Registrarse</button>
                         </center>

                    {{Form::close()}}
                  </section>
                  </div>
                  <div id="concurso">
                    <section id="content2" style="text-align:left;">
                     {!! Form::open(['method'=>'POST','route'=>'concurso.store','files' => 'true']) !!}
                     <div class="form-group">
                            <label for="" id="competencia">Concurso:</label>
                           <select name="competition" class="form-control">
                              <option value="Seleccionar">Seleccionar</option>
                              <option value="Atletismo">ATLETISMO</option>
                              <option value="Caminata minera">CAMINATA MINERA</option>
                              <option value="Copa minera">COPA MINERA</option>
                              <option value="Geología estructural">GEOLOGÍA ESTRUCTURAL</option>
                              <option value="Geomecanica">GEOMECÁNICA</option>
                              <option value="Levantamiento topográfico">LEVANTAMIENTO TOPOGRÁFICO</option>
                              <option value="Miss y mister">MISS Y MISTER</option>
                              <option value="Noche Cultural">NOCHE CULTURAL</option>
                              <option value="Nueva Sede">NUEVA SEDE</option>
                              <option value="Perforación y voladura">PERFORACIÓN Y VOLADURA</option>
                              <option value="Proyectos de investigación">PROYECTOS DE INTEGRACIÓN</option>
                              <option value="Reconocimineto de minerales">RECONOCIMIENTO DE MINERALES</option>
                              <option value="Rescate minero">RESCATE MINERO</option>
                              <option value="Software Minero">SOFTWARE MINERO</option>
                            
                           </select>
                     </div>
                     <div class="form-group" >
                        <label for="">Dni:</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="dni" required="true">
                      </div>
                    <div class="form-group">
                        <label for="">Nombres y Apellidos:</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" required="true">
                      </div>

                     <div class="form-group">
                        <label for="">Email:</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="" name="email" required="true">
                      </div>
                     

                     <div class="form-group">
                            <label for="" id="university">Universidad:</label>
                           <select name="university" class="form-control">
                              <option value="Seleccionar">Seleccionar</option>
                              <option value="UNIVERSIDAD NACIONAL SANTIAGO ANTUNEZ DE MAYOLO">UNIVERSIDAD NACIONAL SANTIAGO ANTUNEZ DE MAYOLO</option>
                              <option value="UNIVERSIDAD NACIONAL DEL SANTA"> UNIVERSIDAD NACIONAL DEL SANTA</option>
                              <option value="UNIVERSIDAD NACIONAL DE TRUJILLO"> UNIVERSIDAD NACIONAL DE TRUJILLO</option>
                              <option value="UNIVERSIDAD NACIONAL DE INGENIERIA"> UNIVERSIDAD NACIONAL DE INGENIERIA</option>
                              <option value="UNIVERSIDAD LOS ANGELES DE CHIMBOTE"> UNIVERSIDAD LOS ANGELES DE CHIMBOTE</option>
                              <option value="UNIVERSIDAD SAN PEDRO"> UNIVERSIDAD SAN PEDRO</option>
                              <option value="UNIVERSIDAD PRIVADA ANTENOR ORREGO"> UNIVERSIDAD PRIVADA ANTENOR ORREGO</option>
                              <option value="PONTIFICIA UNIVERSIDAD CATOLICA DEL PERU"> PONTIFICIA UNIVERSIDAD CATOLICA DEL PERU</option>
                              <option value="UNIVERSIDAD NACIONAL DE CAJAMARCA"> UNIVERSIDAD NACIONAL DE CAJAMARCA</option>
                              <option value="UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS"> UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS</option>
                              <option value="UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN"> UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN</option>
                              <option value="UNIVERSIDAD PERUANA DE ARTE ORVAL"> UNIVERSIDAD PERUANA DE ARTE ORVAL</option>
                              <option value="UNIVERSIDAD CIENTIFICA DEL SUR"> UNIVERSIDAD CIENTIFICA DEL SUR</option>
                              <option value="UNIVERSIDAD FEMENINA DEL SAGRADO CORAZON"> UNIVERSIDAD FEMENINA DEL SAGRADO CORAZON</option>
                              <option value="UNIVERSIDAD PRIVADA NORBERT WIENER"> UNIVERSIDAD PRIVADA NORBERT WIENER</option>
                              <option value="UNIVERSIDAD PRIVADA DEL NORTE"> UNIVERSIDAD PRIVADA DEL NORTE</option>
                              <option value="UNIVERSIDAD NACIONAL JOSE FAUSTINO SANCHEZ CARRION"> UNIVERSIDAD NACIONAL JOSE FAUSTINO SANCHEZ CARRION</option>
                              <option value="UNIVERSIDAD NACIONAL HERMILIO VALDIZAN"> UNIVERSIDAD NACIONAL HERMILIO VALDIZAN</option>
                              <option value="UNIVERSIDAD ANTONIO RUIZ DE MONTOYA"> UNIVERSIDAD ANTONIO RUIZ DE MONTOYA</option>
                              <option value="UNIVERSIDAD MARCELINO CHAMPAGNAT"> UNIVERSIDAD MARCELINO CHAMPAGNAT</option>
                              <option value="UNIVERSIDAD NACIONAL DE PIURA"> UNIVERSIDAD NACIONAL DE PIURA</option>
                              <option value="UNIVERSIDAD NACIONAL DEL CALLAO"> UNIVERSIDAD NACIONAL DEL CALLAO</option>
                              
                              <option value="UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS"> UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS</option>
                              <option value="UNIVERSIDAD CIENTIFICA DEL SUR"> UNIVERSIDAD CIENTIFICA DEL SUR</option>
                              <option value="UNIVERSIDAD CONTINENTAL"> UNIVERSIDAD CONTINENTAL</option>
                              <option value="UNIVERSIDAD NACIONAL SAN AGUSTIN"> UNIVERSIDAD NACIONAL SAN AGUSTIN</option>
                              <option value="UNIVERSIDAD CESAR VALLEJO"> UNIVERSIDAD CESAR VALLEJO</option>
                              <option value="UNIVERSIDAD PRIVADA NORBERT WIENER"> UNIVERSIDAD PRIVADA NORBERT WIENER</option>
                              <option value="UNIVERSIDAD NACIONAL FEDERICO VILLARREAL"> UNIVERSIDAD NACIONAL FEDERICO VILLARREAL</option>
                              <option value="ASOCIACION CULTURAL PERUANO BRITANICA"> ASOCIACION CULTURAL PERUANO BRITANICA</option>
                              <option value="UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN"> UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN</option>
                              <option value="UNIVERSIDAD NACIONAL DE SAN ANTONIO ABAD DEL CUSCO"> UNIVERSIDAD NACIONAL DE SAN ANTONIO ABAD DEL CUSCO</option>
                              <option value="UNIVERSIDAD NACIONAL PEDRO RUIZ GALLO"> UNIVERSIDAD NACIONAL PEDRO RUIZ GALLO</option>
                              <option value="CIBERTEC"> CIBERTEC</option>
                              <option value="UNIVERSIDAD NACIONAL DANIEL ALCIDES CARRION"> UNIVERSIDAD NACIONAL DANIEL ALCIDES CARRION</option>
                              <option value="UNIVERSIDAD NACIONAL DE MOQUEGUA"> UNIVERSIDAD NACIONAL DE MOQUEGUA</option>
                              <option value="UNIVERSIDAD PERUANA CAYETANO HEREDIA"> UNIVERSIDAD PERUANA CAYETANO HEREDIA</option>
                              <option value="UNIVERSIDAD PRIVADA SAN JUAN BAUTISTA"> UNIVERSIDAD PRIVADA SAN JUAN BAUTISTA</option>
                              <option value="UNIVERSIDAD NACIONAL DEL ALTIPLANO"> UNIVERSIDAD NACIONAL DEL ALTIPLANO</option>
                              <option value="UNIVERSIDAD NACIONAL SAN LUIS GONZAGA DE ICA"> UNIVERSIDAD NACIONAL SAN LUIS GONZAGA DE ICA</option>
                              <option value="UNIVERSIDAD TECNOLOGICA DEL PERU (UTP)"> UNIVERSIDAD TECNOLOGICA DEL PERU (UTP)</option>
                              <option value="UNIVERSIDAD NACIONAL MICAELA BASTIDAS DE APURIMAC"> UNIVERSIDAD NACIONAL MICAELA BASTIDAS DE APURIMAC</option>
                              <option value="OTROS">OTROS</option>
                              <option value="Profesional">Soy egresado</option>
                           </select>
                     </div>
                     </br>

                     <div class="form-group">
                            <label for="" id="lugar">Lugar de Procedencia:</label>
                           <select name="place" class="form-control">
                              <option value="Seleccionar">Seleccionar</option>
                              <option value=" AMAZONAS"> AMAZONAS</option>
                              <option value=" ANCASH"> ANCASH</option>
                              <option value=" APURIMAC"> APURIMAC</option>
                              <option value=" AREQUIPA"> AREQUIPA</option>
                              <option value=" AYACUCHO"> AYACUCHO</option>
                              <option value=" CAJAMARCA"> CAJAMARCA</option>
                              <option value=" CALLAO"> CALLAO</option>
                              <option value=" CUSCO"> CUSCO</option>
                              <option value=" HUANCAVELICA"> HUANCAVELICA</option>
                              <option value=" HUANUCO"> HUANUCO</option>
                              <option value=" ICA"> ICA</option>
                              <option value=" JUNIN"> JUNIN</option>
                              <option value=" LA LIBERTAD"> LA LIBERTAD</option>
                              <option value=" LAMBAYEQUE"> LAMBAYEQUE</option>
                              <option value=" LIMA METROPOLITANA"> LIMA METROPOLITANA</option>
                              <option value=" LIMA (REGIÓN)"> LIMA (REGIÓN)</option>
                              <option value=" LORETO"> LORETO</option>
                              <option value=" MADRE DE DIOS"> MADRE DE DIOS</option>
                              <option value=" MOQUEGUA"> MOQUEGUA</option>
                              <option value=" PASCO"> PASCO</option>
                              <option value=" PIURA"> PIURA</option>
                              <option value=" PUNO"> PUNO</option>
                              <option value=" SAN MARTIN"> SAN MARTIN</option>
                              <option value=" TACNA"> TACNA</option>
                              <option value=" TUMBES"> TUMBES</option>
                              <option value=" UCAYALI"> UCAYALI</option>
        
                              <option value="OTRO">OTRO</option>
                           </select>
                     </div>
                    
                     <div class="form-group">
                        <label for="">Voucher:</label>
                        {!! form::file('voucher',array('required'=>'true','class'=>'form-control'),['class' => 'form-control']) !!}
                      </div>

                     <center>
                      <button type="submit" class="btn btn-warning" >Registrarse</button>
                     </center>

                 {{Form::close()}}
                 </section>
              </div>
              
            </div>
              
              
                            
                               
                        </div>
                        
                        
                        
                    </div>
                



        



@endsection



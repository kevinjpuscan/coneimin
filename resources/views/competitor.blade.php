@extends('layouts.dashboard')

@section('title',' Participantes')
@section('menuvalue','4')
@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="header">
				<h4 class="title">Inscritos a Concursos</h4>
				<p class="category">Personas representantes y/o participantes a los diferentes concursos</p>
			</div>
			<div class="content">

            
            <p class="category"><strong>{{$total}}</strong> inscrito(s) | <strong>{{$porConfirmar}}</strong> Por confirmar</p>
				                <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>Concurso</th>
                                        <th>DNI</th>
                                    	<th>Nombres y Apellidos</th>
                                    	<th>Universidad</th>
                                    	<th>Lugar de Procedencia</th>
                                    	<th>Estado</th>
                                    	<th>voucher</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        @foreach($competidores as $competidor)
                                        	<td>{{$competidor->competition}}</td>
                                            <td>{{$competidor->dni}}</td>
                                            <td>{{$competidor->name}}</td>
                                            <td>{{$competidor->university}}</td>
                                            <td>{{$competidor->place}}</td>
                                            <td class="state{{$competidor->id}}">
                                             @if ($competidor->state==='POR CONFIRMAR')
                                                <button class="btn btn-success" id="{{$competidor->id}}" onclick="confirmar(this);">Confirmar</button>

                                             @else
                                                <p>Confirmado</p>
                                             @endif

                                            </td>

                                            <td><a href="#" data-toggle="modal" data-target="#myModal{{$competidor->id}}">
                                                <span class="pe-7s-look" aria-hidden="true"> Ver        
                                            </a>    </td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>

                                {!! $competidores->render() !!}

                                <center> 
                                <a href="{{route('reporteConcursantes')}}" class="btn btn-success">Exportar a Excel</a>
                                </center>

                            </div>
                                      
			</div>

		</div>
	</div>

@endsection


@foreach($competidores as $competidor)
    
    <div class="modal fade" id="myModal{{$competidor->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{{$competidor->name}}</h4>
      </div>
      <div class="modal-body">
      <center>
        <img src="img/{{$competidor->image}}">
          
      </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

        <div class="state{{$competidor->id}}" style="float: right; margin-left: 10px;">
            
             @if ($competidor->state==='POR CONFIRMAR')
                                            
            <button type="button" class="btn btn-success" id="{{$competidor->id}}" onclick="confirmar(this);">Confirmar Registro</button>
            @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
    
@section('scripts')
<script type="text/javascript">
        function confirmar(btn){
            var ruta = "http://localhost:8000/coneimin/public/concurso/"+btn.id+"/editar"; 

        $.get(ruta,function(res){
            var id=".state"+res.id;
            $(id).empty();
            $(id).text(res.state);

        });
      }
</script>
@endsection

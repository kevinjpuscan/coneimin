@extends('layouts.public')

@section('title',' Consulta')



@section('content')
<div class='container'>
            <div class='slogan'>
                <div class='ten columns'>
                    <h1>Consulta tu registro</h1>
                    <h2>Verifica que tu registro ha sido confirmado y evita problemas</h2>
                </div>
                <div class='six columns'>
                    <h4>Ingresa tu DNI:</h4>
                    <input id="dni" type="text" name="dni" ="" style="width: 80%; font-size: 20px">
                    <a href="#" class="button medium green" onclick="consultar();">Consultar Registro</a>
                    
                </div>

            </div>
        </div>  
    </header>


    <center>
        <div class="container" style="position: relative;">
            <div class="sixteen columns">
                
                
                   <div id="resultado" style="margin: 20px 20px;">
                       
                        <div id="registrado">
                            
                        </div>

                       <div id="noregistrado" style="display: none;">
                           <h3>El DNI no está registrado,</br> registrate <a style='color:orange' href='http://localhost:8000/coneimin/public/registro'>aquí</a></h3>
                       </div>
                   </div>
            </div>
            
            
            
        </div>
    

    </center>

        



@endsection

@section('scripts')

 <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script type="text/javascript">
	function consultar(){
		var dni = $('#dni').val(); 
		var ruta = "http://localhost:8000/coneimin/public/consultar/"+dni; 

            $('#registrado').empty();
            $('#noregistrado').css('display','none');

        $.get(ruta,function(res){
            

            var texto="<h1>"+res.state+"</h1></br><h4>"+res.name+"</h4><h4>"+res.university+"</h4>";
            $('#registrado').html(texto);

        }).fail(function(){
        	

            $('#noregistrado').removeAttr('style');

        });
	}
</script>

@endsection

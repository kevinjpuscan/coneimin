@extends('layouts.dashboard')

@section('title',' Usuarios')
@section('menuvalue','2')
@section('content')
                

    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" style="float:right;">Nuevo Usuario</button>
                <h4 class="title">Lista de usuarios registrados</h4>
                <p class="category">Estas son las personas autorizadas para utilizar el sistema</p>





			</div>
			<div class="content">
				                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        
                                    	<th>Nombres y Apellidos</th>
                                    	<th>Correo electrónico</th>
                                    	<th>Acción</th>
                                    	
                                    </thead>
                                    <tbody>

                                    @foreach($usuarios as $usuario)
                                        <tr>
                                            
                                            <td>{{$usuario->name}}</td>
                                            <td>{{$usuario->email}}</td>
                                            <td><a href="">
                                                <span class="pe-7s-delete-user" aria-hidden="true"> Eliminar        
                                            </a>    </td>
                                        </tr>
                                        @endforeach    
                                        

                                                                                                                                                             
                                    </tbody>
                                </table>

                            </div>
			</div>

		</div>
	</div>


 
@endsection

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Nuevo Usuario</h4>
                      </div>
                      <div class="modal-body">

                         {!! Form::open(['method'=>'POST','route'=>'usuario.store']) !!}
                        <div class="form-group">
                            <label for="">Nombres y Apellidos:</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" required="true">
                          </div>

                         <div class="form-group">
                            <label for="">Email:</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="email" required="true">
                          </div>

                          <div class="form-group">
                            <label for="">Contraseña:</label>
                            <input type="password" class="form-control" id="exampleInputEmail1" placeholder="" name="password" required="true">
                          </div>



                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success">Guardar</button>
                      </div>

                      {!!Form::close()!!}
                    </div>
                  </div>
                </div>

@section('scripts')
@endsection
@extends('layouts.dashboard')

@section('title',' Inicio')

@section('menuvalue','1')

@section('content')
	<div class="col-md-12">
		<div class="card col-md-12">
			<div class="header">
				<h4 class="title">Bienvenido al administrador de registros del CONEIMIN 2017</h4>
				<p class="category">Información general</p>
				<div class="content col-md-12">
					
					<center>
						<div class="row col-md-12 ">

								<h1>Incritos</h1>

								<div class="col-md-6 caja">
									<h1 class="dato">{{count($asistentes)}}<br><p>Inscritos</p></h1>
								</div>

								<div class="col-md-6 caja ">
									<h1 class="dato">{{count($asistentes->where('state', 'POR CONFIRMAR'))}}<br><p>Por Confirmar</p></h1>
								</div>

						</div>
					</center>

					<div class="row col-md-12 ">
						<center>
							<h1>Concursos</h1>
							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Atletismo'))}}<br><p>ATLETISMO</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Caminata minera'))}}<br><p>CAMINATA MINERA</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Copa minera'))}}<br><p>COPA MINERA</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Geología estructural'))}}<br><p>GEOLOGÍA ESTRUCTURAL</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Geomecanica'))}}<br><p>GEOMECÁNICA</p></h1>
							</div>


							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Miss y mister'))}}<br><p>MISS Y MISTER</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Noche Cultural'))}}<br><p>NOCHE CULTURAL</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Nueva Sede'))}}<br><p>NUEVA SEDE</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Rescate minero'))}}<br><p>RESCATE MINERO</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Software Minero'))}}<br><p>SOFTWARE MINERO</p></h1>
							</div>
							
							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Proyectos de investigación'))}}<br><p>PROYECTOS DE INVESTIGACIÓN</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Reconocimineto de minerales'))}}<br><p>RECONOCIMIENTO DE MINERALES</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Levantamiento topográfico'))}}<br><p>LEVANTAMIENTO TOPOGRÁFICO</p></h1>
							</div>

							<div class="col-md-3 caja">
								<h1 class="dato">{{count($competidores->where('competition','Perforación y voladura'))}}<br><p>PERFORACIÓN Y VOLADURA</p></h1>
							</div>

						</center>
					</div>

				</div>
			</div>

		</div>
	</div>

@endsection


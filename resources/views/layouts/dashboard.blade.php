<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	 <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
	 
	<input id="menuvalue" type="hidden" name="" value="@yield('menuvalue')">

<div class="wrapper">
    <div class="sidebar" data-color="orange" data-image="assets/img/mineria.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.xiiiconeimin.com" class="simple-text">
                    CONEIMIN
                </a>
            </div>

            <ul class="nav">
                <li id="element1">
                    <a href="{{Route('home')}}">
                        <i class="pe-7s-graph"></i>
                        <p>Inicio</p>
                    </a>
                </li>
                <li id="element2" >
                    <a href="{{Route('usuario.index')}}">
                        <i class="pe-7s-user"></i>
                        <p>Usuarios</p>
                    </a>
                </li>
                <li id="element3" >
                    <a href="{{Route('asistente.index')}}">
                        <i class="pe-7s-note2"></i>
                        <p>Participantes</p>
                    </a>
                </li>
                <li id="element4" >
                    <a href="{{Route('concurso.index')}}">
                        <i class="pe-7s-medal"></i>
                        <p>Concursos</p>
                    </a>
                </li>
               
			
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                   
                </div>
                <div class="collapse navbar-collapse">
                    

                    <ul class="nav navbar-nav navbar-right">
                         <li>
                           <a href="">
                               <p> {{ Auth::user()->name }}</p>
                            </a>
                        </li>
                       
                        <li>
                            <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                        </li>
						<li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">

            @yield('content')


            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
              
                <p class="copyright pull-right">

                    &copy; <script>document.write(new Date().getFullYear())</script> Desarrollado por <a href="https://www.linkedin.com/in/kevinpuscan/" target="_blank">Kevin Puscán</a>
                </p>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>
   

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

	<script type="text/javascript">
		$(function() {
   			

   			var valor=$('#menuvalue').val();
   			var elemento;
   			if(valor==1){
   				elemento=$("#element1");
   			}
   			if(valor==2){
   				elemento=$("#element2");
   			}
   			if(valor==3){
   				elemento=$("#element3");
   			}
   			if(valor==4){
   				elemento=$("#element4");
   			}	

   			elemento.addClass("active");
		})

	</script>

	@yield('scripts')

</html>

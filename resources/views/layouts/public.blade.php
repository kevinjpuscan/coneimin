<!-- =============================================== -->
<!-- =                                             = -->
<!-- =                Keyners	                   = -->
<!-- =                                             = -->
<!-- =          http://keyners.com/	               = -->
<!-- =============================================== -->


<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>
	<meta name="description" content="">
	<meta name="author" content=" Made by Keyners">
    <meta http-equiv="X-UA-Compatible" content="IE=9" />


	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- PT Sans -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

	<!-- Crete Roung -->
	<link href='http://fonts.googleapis.com/css?family=Crete+Round&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

	<!-- CSS
  ================================================== -->
  	<link rel="stylesheet" href="consulta/css/reset.css">
	<link rel="stylesheet" href="consulta/css/base.css">
	<link rel="stylesheet" href="consulta/css/skeleton.css">
	<link rel="stylesheet" href="consulta/css/layout.css">

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- Latest compiled and minified CSS -->


	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
	<script type="text/javascript" src="{{asset('consulta/js/validate.js')}}"></script>
	<script type="text/javascript" src="{{asset('consulta/js/fancybox/jquery.fancybox-1.3.4.pack.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{asset('consulta/js/fancybox/jquery.fancybox-1.3.4.css')}}" media="screen" />
	<script type="text/javascript">
		$(document).ready(function() {

	
				$("a[rel=example_group]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});
		});
	</script>

	@yield('scriptSuperior')

</head>
<body>


	<header>			
		<nav>
			<div class='container'>
				<div class='five columns logo'>
					<a href='#'>CONEIMIN</a>
				</div>

				<div class='eleven columns'>
					<ul class='mainMenu'>
						<li><a href="#" title='Home'>Coneimin</a></li>
						<li><a href="{{Route('registro')}}" title='About us'>Registro</a></li>
						<li><a href="{{Route('asistente.consulta')}}" title='Pricing'>Consulta</a></li>
						
					</ul>
				</div>
			</div>
		</nav>

		



		@yield('content')


	<footer style="height: 40px">
		<div class='container'>
				<center>
					
						<h5>XIII CONEIMIN</h5>
						<p>Cajamarca 2017</p>
						<p>Desarrollado por: <a target="_blank" href='https://www.linkedin.com/in/kevinpuscan/'>Kevin Puscán</a></p>
					
					
				</center>

		</div>
	</footer>



	<script type="text/javascript">
		var toper = $('a#top');


         toper.click(function(){
        	$('html, body').animate({scrollTop:0}, 500);	        	
        	return false;
    	}); 
	</script>


</body>

@yield('scripts')
</html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>XIII CONEIMIN 2017 | Congreso Nacional de Estudiantes de Ingeniería de Minas</title>
    <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,300|Raleway:300,400,900,700italic,700,300,600">
    <link rel="stylesheet" type="text/css" href="baker/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="baker/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="baker/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="baker/css/animate.css">
    <link rel="stylesheet" type="text/css" href="baker/css/style.css">
    

  </head>
  <body>

    <div class="loader"></div>
    <div id="myDiv">
    <!--HEADER-->
    <div class="header">
      <div class="bg-color">
        <header id="main-header">
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">XIII<span class="logo-dec"> CONEIMIN</span></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="#main-header">Inicio</a></li>
                <li class=""><a href="#feature">Acerca de</a></li>
                <li class=""><a href="#service">Inscripción</a></li>
                <li class=""><a href="#portfolio">Turismo</a></li>
                <li class=""><a href="#testimonial">Ponentes</a></li>
                <li class=""><a href="#blog">Concursos</a></li>
                <li class=""><a href="#program">Actividades</a></li>
              </ul>
            </div>
          </div>
        </nav>
        </header>
        <div class="wrapper">
        <div class="container">
          <div class="row">
            <div class="banner-info text-center wow fadeIn delay-05s">
             </br></br> </br>
              <h2 class="bnr-sub-title">XIII CONEIMIN</h2>
              <p class="bnr-para">Congreso Nacional de Estudiantes de Ingeniería de Minas</br><strong>Del 6 al 11 de noviembre</strong> </p>
              <div class="brn-btn">
                <a href="#service" class="btn btn-download">Inscribirse</a>
                <a href="#consulta" class="btn btn-more">Consultar inscripción</a>
              </div>
              <div class="overlay-detail">
                <a href="#feature"><i class="fa fa-angle-down"></i></a>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
    <!--/ HEADER-->
    <!---->
    <section id="feature" class="section-padding wow fadeIn delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="baker/img/ser01.png">
              </div>
              <h3 class="pad-bt15">Ponencias</h3>
              <p>Ponentes nacionales e internacionales de muy alta calidad que compartiran sus conocimientos y experiencia.</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="baker/img/ser02.png">
              </div>
              <h3 class="pad-bt15">Talleres</h3>
              <p>Los mejores talleres realizados bajo la guía de los mejores profeionales en el tema.</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="baker/img/ser03.png">
              </div>
              <h3 class="pad-bt15">Concursos</h3>
              <p>Diferentes concursos que servirán para mejorar y demostrar tus habilidades.</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="baker/img/turismo.jpg">
              </div>
              <h3 class="pad-bt15">Turismo</h3>
              <p>Cajamarca cuenta con diversos lugares turísticos, definitivamente un punto imperdible si eres amante del turismo.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!---->
    <!---->
    <section id="service" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15">INSCRIBETE YA</h2>
            <p class="sub-title pad-bt15">No te pierdas esta increible experiencia.<br> Pagos: <strong>Interbank</strong>  cuenta: <strong>7133101560067</strong> <br>Email: inscripciones.xiiiconeimin2017@gmail.com  </p>
            
            <hr class="bottom-line">
          </div>

        
          <div class="col-md-6" style="border-right:solid 1px rgba(64,75,97,0.9); padding: 0px 40px;">
            <center><h3>CONGRESO</h3><h4>Estudiante: 100 Soles</h4><h4>Egresado: 150 Soles</h4></center>

            <section>
               {!! Form::open(['method'=>'POST','route'=>'asistente.store','files' => 'true']) !!}
                         <div class="form-group" >
                            <label for="">Dni:</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="dni" required="true">
                          </div>
                        <div class="form-group">
                            <label for="">Nombres y Apellidos:</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" required="true">
                          </div>

                         <div class="form-group">
                            <label for="">Email:</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="" name="email" required="true">
                          </div>
                         

                     <div class="form-group">
                            <label for="" id="university">Universidad:</label>
                           <select name="university" class="form-control">
                              <option value="Seleccionar">Seleccionar</option>
                              <option value="UNIVERSIDAD NACIONAL SANTIAGO ANTUNEZ DE MAYOLO">UNIVERSIDAD NACIONAL SANTIAGO ANTUNEZ DE MAYOLO</option>
                              <option value="UNIVERSIDAD NACIONAL DEL SANTA"> UNIVERSIDAD NACIONAL DEL SANTA</option>
                              <option value="UNIVERSIDAD NACIONAL DE TRUJILLO"> UNIVERSIDAD NACIONAL DE TRUJILLO</option>
                              <option value="UNIVERSIDAD NACIONAL DE INGENIERIA"> UNIVERSIDAD NACIONAL DE INGENIERIA</option>
                              <option value="UNIVERSIDAD LOS ANGELES DE CHIMBOTE"> UNIVERSIDAD LOS ANGELES DE CHIMBOTE</option>
                              <option value="UNIVERSIDAD SAN PEDRO"> UNIVERSIDAD SAN PEDRO</option>
                              <option value="UNIVERSIDAD PRIVADA ANTENOR ORREGO"> UNIVERSIDAD PRIVADA ANTENOR ORREGO</option>
                              <option value="PONTIFICIA UNIVERSIDAD CATOLICA DEL PERU"> PONTIFICIA UNIVERSIDAD CATOLICA DEL PERU</option>
                              <option value="UNIVERSIDAD NACIONAL DE CAJAMARCA"> UNIVERSIDAD NACIONAL DE CAJAMARCA</option>
                              <option value="UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS"> UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS</option>
                              <option value="UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN"> UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN</option>
                              <option value="UNIVERSIDAD PERUANA DE ARTE ORVAL"> UNIVERSIDAD PERUANA DE ARTE ORVAL</option>
                              <option value="UNIVERSIDAD CIENTIFICA DEL SUR"> UNIVERSIDAD CIENTIFICA DEL SUR</option>
                              <option value="UNIVERSIDAD FEMENINA DEL SAGRADO CORAZON"> UNIVERSIDAD FEMENINA DEL SAGRADO CORAZON</option>
                              <option value="UNIVERSIDAD PRIVADA NORBERT WIENER"> UNIVERSIDAD PRIVADA NORBERT WIENER</option>
                              <option value="UNIVERSIDAD PRIVADA DEL NORTE"> UNIVERSIDAD PRIVADA DEL NORTE</option>
                              <option value="UNIVERSIDAD NACIONAL JOSE FAUSTINO SANCHEZ CARRION"> UNIVERSIDAD NACIONAL JOSE FAUSTINO SANCHEZ CARRION</option>
                              <option value="UNIVERSIDAD NACIONAL HERMILIO VALDIZAN"> UNIVERSIDAD NACIONAL HERMILIO VALDIZAN</option>
                              <option value="UNIVERSIDAD ANTONIO RUIZ DE MONTOYA"> UNIVERSIDAD ANTONIO RUIZ DE MONTOYA</option>
                              <option value="UNIVERSIDAD MARCELINO CHAMPAGNAT"> UNIVERSIDAD MARCELINO CHAMPAGNAT</option>
                              <option value="UNIVERSIDAD NACIONAL DE PIURA"> UNIVERSIDAD NACIONAL DE PIURA</option>
                              <option value="UNIVERSIDAD NACIONAL DEL CALLAO"> UNIVERSIDAD NACIONAL DEL CALLAO</option>
                              
                              <option value="UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS"> UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS</option>
                              <option value="UNIVERSIDAD CIENTIFICA DEL SUR"> UNIVERSIDAD CIENTIFICA DEL SUR</option>
                              <option value="UNIVERSIDAD CONTINENTAL"> UNIVERSIDAD CONTINENTAL</option>
                              <option value="UNIVERSIDAD NACIONAL SAN AGUSTIN"> UNIVERSIDAD NACIONAL SAN AGUSTIN</option>
                              <option value="UNIVERSIDAD CESAR VALLEJO"> UNIVERSIDAD CESAR VALLEJO</option>
                              <option value="UNIVERSIDAD PRIVADA NORBERT WIENER"> UNIVERSIDAD PRIVADA NORBERT WIENER</option>
                              <option value="UNIVERSIDAD NACIONAL FEDERICO VILLARREAL"> UNIVERSIDAD NACIONAL FEDERICO VILLARREAL</option>
                              <option value="ASOCIACION CULTURAL PERUANO BRITANICA"> ASOCIACION CULTURAL PERUANO BRITANICA</option>
                              <option value="UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN"> UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN</option>
                              <option value="UNIVERSIDAD NACIONAL DE SAN ANTONIO ABAD DEL CUSCO"> UNIVERSIDAD NACIONAL DE SAN ANTONIO ABAD DEL CUSCO</option>
                              <option value="UNIVERSIDAD NACIONAL PEDRO RUIZ GALLO"> UNIVERSIDAD NACIONAL PEDRO RUIZ GALLO</option>
                              <option value="CIBERTEC"> CIBERTEC</option>
                              <option value="UNIVERSIDAD NACIONAL DANIEL ALCIDES CARRION"> UNIVERSIDAD NACIONAL DANIEL ALCIDES CARRION</option>
                              <option value="UNIVERSIDAD NACIONAL DE MOQUEGUA"> UNIVERSIDAD NACIONAL DE MOQUEGUA</option>
                              <option value="UNIVERSIDAD PERUANA CAYETANO HEREDIA"> UNIVERSIDAD PERUANA CAYETANO HEREDIA</option>
                              <option value="UNIVERSIDAD PRIVADA SAN JUAN BAUTISTA"> UNIVERSIDAD PRIVADA SAN JUAN BAUTISTA</option>
                              <option value="UNIVERSIDAD NACIONAL DEL ALTIPLANO"> UNIVERSIDAD NACIONAL DEL ALTIPLANO</option>
                              <option value="UNIVERSIDAD NACIONAL SAN LUIS GONZAGA DE ICA"> UNIVERSIDAD NACIONAL SAN LUIS GONZAGA DE ICA</option>
                              <option value="UNIVERSIDAD TECNOLOGICA DEL PERU (UTP)"> UNIVERSIDAD TECNOLOGICA DEL PERU (UTP)</option>
                              <option value="UNIVERSIDAD NACIONAL MICAELA BASTIDAS DE APURIMAC"> UNIVERSIDAD NACIONAL MICAELA BASTIDAS DE APURIMAC</option>
                              <option value="OTROS">OTROS</option>
                              <option value="Profesional">Soy egresado</option>
                           </select>
                     </div>

                     <div class="form-group">
                            <label for="" id="lugar">Lugar de Procedencia:</label>
                           <select name="place" class="form-control">
                              <option value="Seleccionar">Seleccionar</option>
                              <option value=" AMAZONAS"> AMAZONAS</option>
                              <option value=" ANCASH"> ANCASH</option>
                              <option value=" APURIMAC"> APURIMAC</option>
                              <option value=" AREQUIPA"> AREQUIPA</option>
                              <option value=" AYACUCHO"> AYACUCHO</option>
                              <option value=" CAJAMARCA"> CAJAMARCA</option>
                              <option value=" CALLAO"> CALLAO</option>
                              <option value=" CUSCO"> CUSCO</option>
                              <option value=" HUANCAVELICA"> HUANCAVELICA</option>
                              <option value=" HUANUCO"> HUANUCO</option>
                              <option value=" ICA"> ICA</option>
                              <option value=" JUNIN"> JUNIN</option>
                              <option value=" LA LIBERTAD"> LA LIBERTAD</option>
                              <option value=" LAMBAYEQUE"> LAMBAYEQUE</option>
                              <option value=" LIMA METROPOLITANA"> LIMA METROPOLITANA</option>
                              <option value=" LIMA (REGIÓN)"> LIMA (REGIÓN)</option>
                              <option value=" LORETO"> LORETO</option>
                              <option value=" MADRE DE DIOS"> MADRE DE DIOS</option>
                              <option value=" MOQUEGUA"> MOQUEGUA</option>
                              <option value=" PASCO"> PASCO</option>
                              <option value=" PIURA"> PIURA</option>
                              <option value=" PUNO"> PUNO</option>
                              <option value=" SAN MARTIN"> SAN MARTIN</option>
                              <option value=" TACNA"> TACNA</option>
                              <option value=" TUMBES"> TUMBES</option>
                              <option value=" UCAYALI"> UCAYALI</option>
        
                              <option value="OTRO">OTRO</option>
                           </select>
                     </div>

                         <div class="form-group">
                            <label for="">Voucher:</label>
                            {!! form::file('voucher',array('required'=>'true','class'=>'form-control'),['class' => 'form-control']) !!}
                          </div>
                         <center>
                            <button type="submit" class="btn btn-warning" >Registrarse</button>
                         </center>

                    {{Form::close()}}
            </section>
          </div>

          <div class="col-md-6" style="padding: 0px 40px;">

            <center><h3>CONCURSO</h3><h4>Equipo: 40 Soles</h4></br></center>
            <section>
               {!! Form::open(['method'=>'POST','route'=>'concurso.store','files' => 'true']) !!}
                     <div class="form-group">
                            <label for="" id="competencia">Concurso:</label>
                           <select name="competition" class="form-control">
                              <option value="Seleccionar">Seleccionar</option>
                              <option value="Atletismo">ATLETISMO</option>
                              <option value="Caminata minera">CAMINATA MINERA</option>
                              <option value="Copa minera">COPA MINERA</option>
                              <option value="Geología estructural">GEOLOGÍA ESTRUCTURAL</option>
                              <option value="Geomecanica">GEOMECÁNICA</option>
                              <option value="Levantamiento topográfico">LEVANTAMIENTO TOPOGRÁFICO</option>
                              <option value="Miss y mister">MISS Y MISTER</option>
                              <option value="Noche Cultural">NOCHE CULTURAL</option>
                              <option value="Nueva Sede">NUEVA SEDE</option>
                              <option value="Perforación y voladura">PERFORACIÓN Y VOLADURA</option>
                              <option value="Proyectos de investigación">PROYECTOS DE INTEGRACIÓN</option>
                              <option value="Reconocimineto de minerales">RECONOCIMIENTO DE MINERALES</option>
                              <option value="Rescate minero">RESCATE MINERO</option>
                              <option value="Software Minero">SOFTWARE MINERO</option>
                            
                           </select>
                     </div>
                     <div class="form-group" >
                        <label for="">Dni:</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="dni" required="true">
                      </div>
                    <div class="form-group">
                        <label for="">Nombres y Apellidos (representante y/o participante):</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" required="true">
                      </div>

                     <div class="form-group">
                        <label for="">Email:</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="" name="email" required="true">
                      </div>
                     

                     <div class="form-group">
                            <label for="" id="university">Universidad:</label>
                           <select name="university" class="form-control">
                              <option value="Seleccionar">Seleccionar</option>
                              <option value="UNIVERSIDAD NACIONAL SANTIAGO ANTUNEZ DE MAYOLO">UNIVERSIDAD NACIONAL SANTIAGO ANTUNEZ DE MAYOLO</option>
                              <option value="UNIVERSIDAD NACIONAL DEL SANTA"> UNIVERSIDAD NACIONAL DEL SANTA</option>
                              <option value="UNIVERSIDAD NACIONAL DE TRUJILLO"> UNIVERSIDAD NACIONAL DE TRUJILLO</option>
                              <option value="UNIVERSIDAD NACIONAL DE INGENIERIA"> UNIVERSIDAD NACIONAL DE INGENIERIA</option>
                              <option value="UNIVERSIDAD LOS ANGELES DE CHIMBOTE"> UNIVERSIDAD LOS ANGELES DE CHIMBOTE</option>
                              <option value="UNIVERSIDAD SAN PEDRO"> UNIVERSIDAD SAN PEDRO</option>
                              <option value="UNIVERSIDAD PRIVADA ANTENOR ORREGO"> UNIVERSIDAD PRIVADA ANTENOR ORREGO</option>
                              <option value="PONTIFICIA UNIVERSIDAD CATOLICA DEL PERU"> PONTIFICIA UNIVERSIDAD CATOLICA DEL PERU</option>
                              <option value="UNIVERSIDAD NACIONAL DE CAJAMARCA"> UNIVERSIDAD NACIONAL DE CAJAMARCA</option>
                              <option value="UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS"> UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS</option>
                              <option value="UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN"> UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN</option>
                              <option value="UNIVERSIDAD PERUANA DE ARTE ORVAL"> UNIVERSIDAD PERUANA DE ARTE ORVAL</option>
                              <option value="UNIVERSIDAD CIENTIFICA DEL SUR"> UNIVERSIDAD CIENTIFICA DEL SUR</option>
                              <option value="UNIVERSIDAD FEMENINA DEL SAGRADO CORAZON"> UNIVERSIDAD FEMENINA DEL SAGRADO CORAZON</option>
                              <option value="UNIVERSIDAD PRIVADA NORBERT WIENER"> UNIVERSIDAD PRIVADA NORBERT WIENER</option>
                              <option value="UNIVERSIDAD PRIVADA DEL NORTE"> UNIVERSIDAD PRIVADA DEL NORTE</option>
                              <option value="UNIVERSIDAD NACIONAL JOSE FAUSTINO SANCHEZ CARRION"> UNIVERSIDAD NACIONAL JOSE FAUSTINO SANCHEZ CARRION</option>
                              <option value="UNIVERSIDAD NACIONAL HERMILIO VALDIZAN"> UNIVERSIDAD NACIONAL HERMILIO VALDIZAN</option>
                              <option value="UNIVERSIDAD ANTONIO RUIZ DE MONTOYA"> UNIVERSIDAD ANTONIO RUIZ DE MONTOYA</option>
                              <option value="UNIVERSIDAD MARCELINO CHAMPAGNAT"> UNIVERSIDAD MARCELINO CHAMPAGNAT</option>
                              <option value="UNIVERSIDAD NACIONAL DE PIURA"> UNIVERSIDAD NACIONAL DE PIURA</option>
                              <option value="UNIVERSIDAD NACIONAL DEL CALLAO"> UNIVERSIDAD NACIONAL DEL CALLAO</option>
                              
                              <option value="UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS"> UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS</option>
                              <option value="UNIVERSIDAD CIENTIFICA DEL SUR"> UNIVERSIDAD CIENTIFICA DEL SUR</option>
                              <option value="UNIVERSIDAD CONTINENTAL"> UNIVERSIDAD CONTINENTAL</option>
                              <option value="UNIVERSIDAD NACIONAL SAN AGUSTIN"> UNIVERSIDAD NACIONAL SAN AGUSTIN</option>
                              <option value="UNIVERSIDAD CESAR VALLEJO"> UNIVERSIDAD CESAR VALLEJO</option>
                              <option value="UNIVERSIDAD PRIVADA NORBERT WIENER"> UNIVERSIDAD PRIVADA NORBERT WIENER</option>
                              <option value="UNIVERSIDAD NACIONAL FEDERICO VILLARREAL"> UNIVERSIDAD NACIONAL FEDERICO VILLARREAL</option>
                              <option value="ASOCIACION CULTURAL PERUANO BRITANICA"> ASOCIACION CULTURAL PERUANO BRITANICA</option>
                              <option value="UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN"> UNIVERSIDAD NACIONAL JORGE BASADRE GROHMANN</option>
                              <option value="UNIVERSIDAD NACIONAL DE SAN ANTONIO ABAD DEL CUSCO"> UNIVERSIDAD NACIONAL DE SAN ANTONIO ABAD DEL CUSCO</option>
                              <option value="UNIVERSIDAD NACIONAL PEDRO RUIZ GALLO"> UNIVERSIDAD NACIONAL PEDRO RUIZ GALLO</option>
                              <option value="CIBERTEC"> CIBERTEC</option>
                              <option value="UNIVERSIDAD NACIONAL DANIEL ALCIDES CARRION"> UNIVERSIDAD NACIONAL DANIEL ALCIDES CARRION</option>
                              <option value="UNIVERSIDAD NACIONAL DE MOQUEGUA"> UNIVERSIDAD NACIONAL DE MOQUEGUA</option>
                              <option value="UNIVERSIDAD PERUANA CAYETANO HEREDIA"> UNIVERSIDAD PERUANA CAYETANO HEREDIA</option>
                              <option value="UNIVERSIDAD PRIVADA SAN JUAN BAUTISTA"> UNIVERSIDAD PRIVADA SAN JUAN BAUTISTA</option>
                              <option value="UNIVERSIDAD NACIONAL DEL ALTIPLANO"> UNIVERSIDAD NACIONAL DEL ALTIPLANO</option>
                              <option value="UNIVERSIDAD NACIONAL SAN LUIS GONZAGA DE ICA"> UNIVERSIDAD NACIONAL SAN LUIS GONZAGA DE ICA</option>
                              <option value="UNIVERSIDAD TECNOLOGICA DEL PERU (UTP)"> UNIVERSIDAD TECNOLOGICA DEL PERU (UTP)</option>
                              <option value="UNIVERSIDAD NACIONAL MICAELA BASTIDAS DE APURIMAC"> UNIVERSIDAD NACIONAL MICAELA BASTIDAS DE APURIMAC</option>
                              <option value="OTROS">OTROS</option>
                              <option value="Profesional">Soy egresado</option>
                           </select>
                     </div>
                     </br>

                     <div class="form-group">
                            <label for="" id="lugar">Lugar de Procedencia:</label>
                           <select name="place" class="form-control">
                              <option value="Seleccionar">Seleccionar</option>
                              <option value=" AMAZONAS"> AMAZONAS</option>
                              <option value=" ANCASH"> ANCASH</option>
                              <option value=" APURIMAC"> APURIMAC</option>
                              <option value=" AREQUIPA"> AREQUIPA</option>
                              <option value=" AYACUCHO"> AYACUCHO</option>
                              <option value=" CAJAMARCA"> CAJAMARCA</option>
                              <option value=" CALLAO"> CALLAO</option>
                              <option value=" CUSCO"> CUSCO</option>
                              <option value=" HUANCAVELICA"> HUANCAVELICA</option>
                              <option value=" HUANUCO"> HUANUCO</option>
                              <option value=" ICA"> ICA</option>
                              <option value=" JUNIN"> JUNIN</option>
                              <option value=" LA LIBERTAD"> LA LIBERTAD</option>
                              <option value=" LAMBAYEQUE"> LAMBAYEQUE</option>
                              <option value=" LIMA METROPOLITANA"> LIMA METROPOLITANA</option>
                              <option value=" LIMA (REGIÓN)"> LIMA (REGIÓN)</option>
                              <option value=" LORETO"> LORETO</option>
                              <option value=" MADRE DE DIOS"> MADRE DE DIOS</option>
                              <option value=" MOQUEGUA"> MOQUEGUA</option>
                              <option value=" PASCO"> PASCO</option>
                              <option value=" PIURA"> PIURA</option>
                              <option value=" PUNO"> PUNO</option>
                              <option value=" SAN MARTIN"> SAN MARTIN</option>
                              <option value=" TACNA"> TACNA</option>
                              <option value=" TUMBES"> TUMBES</option>
                              <option value=" UCAYALI"> UCAYALI</option>
        
                              <option value="OTRO">OTRO</option>
                           </select>
                     </div>
                    
                     <div class="form-group">
                        <label for="">Voucher:</label>
                        {!! form::file('voucher',array('required'=>'true','class'=>'form-control'),['class' => 'form-control']) !!}
                      </div>

                     <center>
                      <button type="submit" class="btn btn-warning" >Registrarse</button>
                     </center>

                 {{Form::close()}}
            </section>
          </div>

         
        </div>
      </div>
    </section>
    <!---->
    <!---->
    <!---->
    <!---->
    <section id="portfolio" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15">Lugares turísticos</h2>
            <p class="sub-title pad-bt15">Disfruta de los mejores lugares turísticos que tiene la ciudad,<br>no te lo puedes perder.</p>
            <hr class="bottom-line">
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
            <figure>
              <img src="baker/img/plaza.jpg" class="img-responsive">
              <figcaption>
                  <h2>Plaza de Armas</h2>
                  <p>Cento de la Ciudad, lugar en el que tuvo lugar el encuentro entre Atahualpa y Francisco Pizarro.</p>
              </figcaption>
            </figure>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
            <figure>
              <img src="baker/img/santaApolonia.jpg" class="img-responsive">
              <figcaption>
                  <h2>Santa Apolonia</h2>
                  <p>Colina de la cual se puede ver toda la ciudad de Cajamarca, se encuentra a unas cuadras de la plaza de armas.</p>
              </figcaption>
            </figure>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
            <figure>
              <img src="baker/img/baños.jpg" class="img-responsive">
              <figcaption>
                  <h2>Baños del Inca</h2>
                  <p>Primera maravilla del Perú, ven y disfruta de aguas termales, sauna, piscina y además del hermoso paisaje.</p>
              </figcaption>
            </figure>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
            <figure>
              <img src="baker/img/cumbe.jpg" class="img-responsive">
              <figcaption>
                  <h2>Cumbe Mayo</h2>
                  <p>Se encuentra a una hora de la ciudad de Cajamarca, famoso por su expectacular bosque de piedras.</p>
              </figcaption>
            </figure>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
            <figure>
              <img src="baker/img/otuzco.jpg" class="img-responsive">
              <figcaption>
                  <h2>Ventanillas de Otuzco</h2>
                  <p>Ventanillas labradas en la roca misma por los antiguos pobladores de Cajamarca.</p>
              </figcaption>
            </figure>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
            <figure>
              <img src="baker/img/collpa.jpg" class="img-responsive">
              <figcaption>
                  <h2>Hacienda la Collpa</h2>
                  <p>Hacienda tradicional de Cajamarca, observa como se realiza el llmado de vacas por su nombre y disrfutar de un hermoso paisaje campestre.</p>
              </figcaption>
            </figure>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
            <figure>
              <img src="baker/img/rescate.jpg" class="img-responsive">
              <figcaption>
                  <h2>Cuarto de Rescate</h2>
                  <p>Cuarto en el que el inca Atahualpa habría pagado su famoso rescate, se encuentra muy cerca de la plaza de armas de la ciudad.</p>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </section>
    <!---->
    <!---->
    <section id="testimonial" class="wow fadeInUp delay-05s">
      <div class="bg-testicolor">
        <div class="container section-padding">

          <div class="row">
            <div class="col-md-12 text-center white">
              <h2 class="service-title pad-bt15">Ponentes</h2>
              <p class="sub-title pad-bt15">Los mejores ponentes nacionales e internacionales.</p>
              <hr class="bottom-line white-bg">
            </div>

            <center>
              
              <div class="col-md-3 campoPonente">
                <img src="baker/img/ponente.jpg">
                <section class="ponente">
                  <p><strong>Nombre del Ponente</strong> <br>Ing. de Minas </p>
                  <div>
                  <a href="" target="_blank"><i class="fa fa-facebook"></i></a>
                  <a href="" target="_blank"><i class="fa fa-twitter"></i></a>
                  <a href="" target="_blank"><i class="fa fa-linkedin"></i></a>
                    
                  </div>
                </section>
              </div>

              <div class="col-md-3 campoPonente">
                <img src="baker/img/ponente.jpg">
                <section class="ponente">
                  <p><strong>Nombre del Ponente</strong> <br>Ing. de Minas </p>
                  <div>
                  <a href="" target="_blank"><i class="fa fa-facebook"></i></a>
                  <a href="" target="_blank"><i class="fa fa-twitter"></i></a>
                  <a href="" target="_blank"><i class="fa fa-linkedin"></i></a>
                    
                  </div>
                </section>
                
              </div>

              <div class="col-md-3 campoPonente">
                <img src="baker/img/ponente.jpg">
                <section class="ponente">
                  <p><strong>Nombre del Ponente</strong> <br>Ing. de Minas </p>
                  <div>
                  <a href="" target="_blank"><i class="fa fa-facebook"></i></a>
                  <a href="" target="_blank"><i class="fa fa-twitter"></i></a>
                  <a href="" target="_blank"><i class="fa fa-linkedin"></i></a>
                    
                  </div>
                </section>
                
              </div>

              <div class="col-md-3 campoPonente">
                <img src="baker/img/ponente.jpg">
                <section class="ponente">
                  <p><strong>Nombre del Ponente</strong> <br>Ing. de Minas </p>
                  <div>
                  <a href="" target="_blank"><i class="fa fa-facebook"></i></a>
                  <a href="" target="_blank"><i class="fa fa-twitter"></i></a>
                  <a href="" target="_blank"><i class="fa fa-linkedin"></i></a>
                    
                  </div>
                </section>
              </div>

            </center>

          </div>
        </div>
      </div>
    </section>
    <!---->
    <section id="blog" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15">Concursos</h2>
            <p class="sub-title pad-bt15">Participa de los concursos y compite con los mejores del país,<br>demuestra de lo que estás hecho.</p>
            <hr class="bottom-line">
          </div>

          <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso01.jpg">
                </div>
                <h3 class="pad-bt15">Atletismo</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso02.jpeg">
                </div>
                <h3 class="pad-bt15">Caminata minera</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso03.jpg">
                </div>
                <h3 class="pad-bt15">Copa Minera</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso04.jpg">
                </div>
                <h3 class="pad-bt15">Geología estructural</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>
          </div>

          <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso05.jpg">
                </div>
                <h3 class="pad-bt15">Geomecánica</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso06.jpg">
                </div>
                <h3 class="pad-bt15">Levantamiento Topográfico</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso07.jpg">
                </div>
                <h3 class="pad-bt15">Miss y mister</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso08.jpg">
                </div>
                <h3 class="pad-bt15">Noche Cultural</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

            
          </div>

          <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;"> 

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso09.jpg">
                </div>
                <h3 class="pad-bt15">Nueva sede</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso10.jpeg">
                </div>
                <h3 class="pad-bt15">Perforación y voladura</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso11.jpg">
                </div>
                <h3 class="pad-bt15">Proyectos de investigación</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso12.jpg">
                </div>
                <h3 class="pad-bt15">Reconocimiento de minerales</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>
          </div>

          <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso13.jpg">
                </div>
                <h3 class="pad-bt15">Rescate Minero</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="wrap-item text-center">
                <div class="item-img">
                  <img src="baker/img/concurso14.jpg">
                </div>
                <h3 class="pad-bt15">Software Minero</h3>
                <a href="#" class="btn btn-primary btn-submit">Descargar bases</a>
              </div>
            </div>

          </div>

          




        </div>
      </div>
    </section>
    <!---->
    <section id="program" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center white">
            <h2 class="service-title pad-bt15">Cronograma de actividades</h2>
            <p class="sub-title pad-bt15">Programa muy bien tu tiempo, organízate y<br>así no te perderas de nada.</p>
            <hr class="bottom-line white-bg">
          </div>

          <!--Inicia programa-->
        
        
          <div class="col-md-12 menuHorario">
            <ul>
              
              <div class="col-md-2"> 
                <li id="tabLunes" class="select" onclick="mostrarLunes();">LUNES 06</li>
              </div>

              <div class="col-md-2">
                <li id="tabMartes" onclick="mostrarMartes();">MARTES 07</li>
              </div>

              <div class="col-md-2">
                <li id="tabMiercoles" onclick="mostrarMiercoles();">MIERCOLES 08</li>
              </div>

              <div class="col-md-2">
                <li id="tabJueves" onclick="mostrarJueves();">JUEVES 09</li>
              </div>

              <div class="col-md-2">
                <li id="tabViernes" onclick="mostrarViernes();">VIERNES 10</li>
              </div>

              <div class="col-md-2">
                <li id="tabSabado" onclick="mostrarSabado();">SÁBADO 11</li>
              </div>

            </ul>


          </div>

          <div class="col-md-12 containerHorario" >
            
                  <div id="lunes" class="dias">

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>8:00 AM - 1:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Entrega de Credenciales, inscripción participantes y concursos extemporáneos (4J) </p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>1:00 PM - 3:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Almuerzo (libre) </p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>3:00 PM - 5:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Entrega de Credenciales, inscripción participantes y concursos extemporáneos (4J) </p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>5:00 PM - 6:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Inauguración del XIII CONEIMIN – Cajamarca 2017. <br>Palabras de inauguración por parte DEL Rector de la UNC. <br>Brindis de honor por parte de Decano de la Facultad de Ingeniería de la Universidad Nacional de Cajamarca.</p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>6:00 PM - 9:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Noche cultural.<br>Palabras de apertura por parte del Director de la Escuela Académico Profesional de Ingeniería de Minas.</p></div>
                    </div>

                  </div>

                  <div id="martes" class="dias" style="display: none;">
                    
                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>8:45 AM - 9:00 AM</p></strong></div>
                      <div class="col-md-8"><p>Apertura del primer día de ponencias magistrales.<br>Palabras de bienvenida por parte del Decano de la Facultad de Ingeniería de la Universidad Nacional de Cajamarca.</p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>9:00 AM - 10:45 AM</p></strong></div>
                      <div class="col-md-8"><p>Ponencia Magistral N° 1 <br> Ponencia Magistral N° 2 </p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>10:45 AM - 11:15 AM</p></strong></div>
                      <div class="col-md-8"><p>Coffee break</p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>11:15 AM - 1:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Ponencia Magistral N° 3. <br>Ponencia Magistral N° 4. </p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>1:00 PM - 3:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Almuerzo (libre)</p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>3:00 PM - 4:45 PM</p></strong></div>
                      <div class="col-md-8"><p>Ponencia Magistral N° 5. <br>Ponencia Magistral N° 6.</p></div>
                    </div>

                     <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>4:45 PM - 5:15 PM</p></strong></div>
                      <div class="col-md-8"><p>Coffee break</p></div>
                    </div>

                     <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>5:15 PM - 6:45 PM</p></strong></div>
                      <div class="col-md-8"><p>Ponencia Magistral N° 7. <br>Ponencia Magistral N° 8.</p></div>
                    </div>

                     <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>6:45 PM - 7:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Conclusiones del primer día de ponencias magistrales a cargo del Vicerrector Académico de la Universidad Nacional de Cajamarca </p></div>
                    </div>

                     <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>7:00 PM - 8:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Exámenes teóricos de los diferentes concursos técnicos.</p></div>
                    </div>

                     <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>8:00 PM - 10:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Concurso Miss & Míster</p></div>
                    </div>

                 
                  </div> 

                  <div id="miercoles" class="dias" style="display: none;">
                      
                      <div class="actividad col-md-12">
                        <div class="col-md-3"><strong><p>8:45 AM - 9:00 AM</p></strong></div>
                        <div class="col-md-8"><p>Apertura del segundo día de ponencias magistrales. <br> Palabras de bienvenida por parte del Director de la Escuela Académico Profesional de Ingeniería de Minas y la comisión organizadora. </p></div>
                      </div>

                      <div class="actividad col-md-12">
                        <div class="col-md-3"><strong><p>9:00 AM - 10:45 AM</p></strong></div>
                        <div class="col-md-8"><p>Ponencia magistral Nº 9.<br> Ponencia magistral Nº 10. </p></div>
                      </div>

                      <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>10:45 AM - 11:15 AM</p></strong></div>
                      <div class="col-md-8"><p>Coffee break</p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>11:15 AM - 1:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Ponencia Magistral N° 11. <br>Ponencia Magistral N° 12. </p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>1:00 PM - 3:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Almuerzo (libre)</p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>3:00 PM - 4:45 PM</p></strong></div>
                      <div class="col-md-8"><p>Foro (primera parte).</p></div>
                    </div>

                     <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>4:45 PM - 5:15 PM</p></strong></div>
                      <div class="col-md-8"><p>Coffee break</p></div>
                    </div>

                     <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>5:15 PM - 6:45 PM</p></strong></div>
                      <div class="col-md-8"><p>Foro (Segunda parte).</p></div>
                    </div>

                     <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>6:45 PM - 7:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Conclusiones del segundo día de ponencias magistrales a cargo del Rector de la Universidad Nacional de Cajamarca </p></div>
                    </div>

                     <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>7:00 PM - 9:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Concurso de Proyectos de Investigación.</p></div>
                    </div>

                     <div class="actividad col-md-12">
                      <p>•  Se desarrollará de forma paralela el ENFEIM (Encuentro Nacional Femenino de Estudiantes de Ingeniería de Minas). Descarga el cronograma <a href="#" target="_blank">aquí</a>.</p>
                    </div>

                  </div> 

                  <div id="jueves" class="dias" style="display: none;">
                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>5:00 AM - 4:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Exámenes prácticos de los diferentes concursos técnicos.</p></div>
                    </div>
                  </div> 

                  <div id="viernes" class="dias" style="display: none;">
                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>4:00 AM - 5:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Visitas técnicas a las diferentes mineras de la región.</p></div>
                    </div>
                  </div> 

                  <div id="sabado" class="dias" style="display: none;">
                    
                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>8:00 AM - 1:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Mañana deportiva.</p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>4:00 PM - 6:00 PM</p></strong></div>
                      <div class="col-md-8"><p>Elección de la Nueva Sede.</p></div>
                    </div>

                    <div class="actividad col-md-12">
                      <div class="col-md-3"><strong><p>8:00 APM - 0:00 AM</p></strong></div>
                      <div class="col-md-8"><p>Noche de cierre.</p></div>
                    </div>


                  </div> 
          </div>




        

          <!--termina programa-->

          
        </div>
      </div>
    </section>
    <!---->
    <!---->

    <section id="consulta" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15">CONSULTA TU INSCRIPCIÓN</h2>
            <p class="sub-title pad-bt15">Ingresa tu DNI y consulta tu inscripción.</p>
            <hr class="bottom-line">
          </div>
          <div class="col-md-12">
              <center>
              <input id="dni" type="text" name="dni" class="form-control" style="font-size: 25px; width: 300px; text-align: center;">
              </br>                 
              <button id="btnConsultar" type="submit" onclick="consultar();" class="btn btn-warning" name="">CONSULTAR</button>
              </center>
            
          </div>



          <div id="resultado" style="margin: 20px 20px;">
                       
                        <div id="registrado">
                            
                        </div>

                       <div id="noregistrado" style="display: none;">
                           <h3>El DNI no está registrado,</br> registrate <a style='color:orange' href='#service'>aquí</a></h3>
                       </div>
                   </div>
         
        </div>
      </div>
    </section>

    <footer id="footer">
      <div class="container">
        <div class="row text-center">
        <img src="baker/img/logo.jpg"><br><br>
          &copy; <script>document.write(new Date().getFullYear())</script> Desarrollado por <a href="https://www.linkedin.com/in/kevinpuscan/" target="_blank">Kevin Puscán</a>
          
        </div>
      </div>
    </footer>
    <!---->
  </div>
    <script src="baker/js/jquery.min.js"></script>
    <script src="baker/js/jquery.easing.min.js"></script>
    <script src="baker/js/bootstrap.min.js"></script>
    <script src="baker/js/wow.js"></script>
    <script src="baker/js/jquery.bxslider.min.js"></script>
    <script src="baker/js/custom.js"></script>
    <script src="baker/contactform/contactform.js"></script>
    <script type="text/javascript">
      function mostrarLunes(){
        limpiar();
        $("#lunes").show();
        $("#tabLunes").addClass('select');
      }
      function mostrarMartes(){
        limpiar();
        $("#martes").show();
        $("#tabMartes").addClass("select");
      }
      function mostrarMiercoles(){
        limpiar();
        $("#miercoles").show();
        $("#tabMiercoles").addClass("select");
      }
      function mostrarJueves(){
        limpiar();
        $("#jueves").show();
        $("#tabJueves").addClass("select");
      }
      function mostrarViernes(){
        limpiar();
        $("#viernes").show();
        $("#tabViernes").addClass("select");
      }
      function mostrarSabado(){
        limpiar();
        $("#sabado").show();
        $("#tabSabado").addClass("select");
      }

      function limpiar(){
        $("#lunes").hide();
        $("#martes").hide();
        $("#miercoles").hide();
        $("#jueves").hide();
        $("#viernes").hide();
        $("#sabado").hide();
        
        $("#tabLunes").removeClass();
        $("#tabMartes").removeClass();
        $("#tabMiercoles").removeClass();
        $("#tabJueves").removeClass();
        $("#tabViernes").removeClass();
        $("#tabSabado").removeClass();
      };
    </script>
    <script type="text/javascript">
  function consultar(){
    var dni = $('#dni').val(); 
    var ruta = "http://localhost:8000/coneimin/public/consultar/"+dni; 

            $('#registrado').empty();
            $('#noregistrado').css('display','none');

        $.get(ruta,function(res){
            

            var texto="<h1>"+res.state+"</h1></br><h4>"+res.name+"</h4><h4>"+res.university+"</h4>";
            $('#registrado').html(texto);

        }).fail(function(){
          

            $('#noregistrado').removeAttr('style');

        });
  }
</script>

  </body>
</html>
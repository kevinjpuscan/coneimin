<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
})->name('inicio');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/usuarios',[
	'uses'=>'UserController@index',
	'as'=>'usuario.index'
	]);


Route::get('/registro',function(){
	return view('register');
})->name('registro');
//rutas de participantes
Route::get('/participantes',[
	'uses'=>'AsistenteController@index',
	'as'=>'asistente.index'
	]);
Route::post('/registro/congreso',[
	'uses'=>'AsistenteController@store',
	'as'=>'asistente.store'
	]);
Route::get('/participantes/{asistente}/editar',[
	'uses'=>'AsistenteController@edit',
	'as'=>'asistente.edit'
	]);
//rutas de concurso
Route::get('/concurso',[
	'uses'=>'ConcursoController@index',
	'as'=>'concurso.index'
	]);
Route::post('/registro/concurso',[
	'uses'=>'ConcursoController@store',
	'as'=>'concurso.store'
	]);
Route::get('/concurso/{competidor}/editar',[
	'uses'=>'ConcursoController@edit',
	'as'=>'concurso.edit'
	]);
//rutas de usuarios
Route::post('/usuarios',[
	'uses'=>'UserController@store',
	'as'=>'usuario.store'
	]);
Route::get('/usuarios/eliminar/{user}', [
   'uses'=>'UserController@destroy',
   'as'=>'usuario.destroy'
]);

//consultar inscripcion
Route::get('/consultar',[
	'uses'=>'AsistenteController@consulta',
	'as'=>'asistente.consulta'
]);

Route::get('/consultar/{dni}',[
	'uses'=>'AsistenteController@consultar',
	'as'=>'asistente.consultar'
]);

Route::get('reporte/participantes',[
	'uses'=>'reporteController@participantes',
	'as'=>'reporteParticipantes'
]);

Route::get('reporte/concursantes',[
	'uses'=>'reporteController@concursantes',
	'as'=>'reporteConcursantes'
]);
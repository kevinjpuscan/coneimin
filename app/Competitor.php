<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competitor extends Model
{
     protected $fillable = [
        'competition','name', 'email', 'dni','university','place','image','state'
    ];
}

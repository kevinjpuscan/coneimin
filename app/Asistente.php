<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistente extends Model
{
     protected $fillable = [
        'name', 'email', 'dni','university','place','image','state'
    ];
}

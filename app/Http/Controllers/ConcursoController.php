<?php

namespace App\Http\Controllers;

use Input;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Image;

use App\Competitor;

class ConcursoController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $competidores = Competitor::paginate(30);
        $total=count(Competitor::All());
        $porConfirmar = count(Competitor::All()->where('state','POR CONFIRMAR'));
        return view('competitor',compact('competidores','total','porConfirmar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
        dd($request);
      }else{

       $file = Input::file('voucher');
   //Creamos una instancia de la libreria instalada   
   $image = \Image::make(\Input::file('voucher'));
   //Ruta donde queremos guardar las imagenes
   $path = public_path().'/img/';
 
   // Guardar Original
   //$image->save($path.$file->getClientOriginalName());
   // Cambiar de tamaño
   //$image->resize(240,200);
   $image->resize(500, null, function ($constraint) {
    $constraint->aspectRatio();
});


   //generamos un identificador
    $carbon = new Carbon();
    $date = $carbon->now()->timestamp;

   // Guardar
   $image->save($path.'img_'.$date.$file->getClientOriginalName());
   
   //generamos la ruta
   $ruta='img_'.$date.$file->getClientOriginalName();

   //Guardamos el asistente en la base de datos
   $competidor = new Competitor([
    'competition'=>$request->get('competition'),
    'dni'=>$request->get('dni'),
    'name'=>$request->get('name'),
    'email'=>$request->get('email'),
    'university'=>$request->get('university'),
    'place'=>$request->get('place'),
    'image'=>$ruta,
    'state'=>'POR CONFIRMAR'
    ]);
  

   $competidor->save();
   
   return redirect()->route('inicio');
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $competidor = Competitor::find($id);
        $competidor->state='CONFIRMADO';
        $competidor->save();

        return response()->json(
                $competidor->toArray()
            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

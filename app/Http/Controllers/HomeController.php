<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Competitor;
use App\Asistente;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $asistentes=Asistente::all();
        $competidores=Competitor::all();

       

        return view('home',compact('asistentes','competidores'));
    }
}

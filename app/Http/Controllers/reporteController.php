<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asistente;
use App\Competitor;

class reporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function concursantes()
    {
         \Excel::create('Concurso', function($excel) {

            $excel->sheet('Concurso', function($sheet) {

                $competidores=Competitor::All();

                $sheet->fromArray($competidores);

            });

        })->export('xls');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function participantes()
    {
        \Excel::create('Inscritos', function($excel) {

            $excel->sheet('Inscritos', function($sheet) {

                $participantes=Asistente::All();

                $sheet->fromArray($participantes);

            });

        })->export('xls');
    }

    
}
